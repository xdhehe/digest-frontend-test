<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Digest Exam</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        body { font: Montserrat; }
    </style>
</head>
<body>
<div class="container mt-5 mb-5">
  <div class="jumbotron">
    <div class="row">
        <div class="col-md-10">
            <h5><b>Digest of:</b>
                <h6 class="text-secondary">{{$digest->title}}</h6>
            </h5>
        </div>

        <div class="col-md-2">
            <h6><b>Author:<span class="text-secondary"> {{$digest->author}}</span></b></h6>
            <h6><b>Subject:<span class="text-secondary"> {{$digest->category}}</span></b></h6>
            <h6><b>Rating:<span class="text-secondary"> {{$digest->rating}}</span></b></h6>
        </div>

        <div class="col-md-9">
            <h5><b>Summary:</b>
                <h6 class="text-secondary">{{$digest->summary}}</h6>
            </h5>

            <h5 class="text-secondary"><b>Doctrine:</b>
                <h6 class="text-secondary">{{$digest->summary}}</h6>
            </h5>

            <h5 class="text-secondary"><b>Facts:</b>
                <h6 class="text-secondary">{{$digest->facts}}</h6>
            </h5>

            <h5 class="text-secondary"><b>Issues_ratio:</b>
                <h6 class="text-secondary">{{$digest->issues_ratio}}</h6>
            </h5>

            <h5 class="text-secondary"><b>Dispositive:</b>
                <h6 class="text-secondary">{{$digest->dispositive}}</h6>
            </h5>

            <h5 class="text-secondary"><b>Other Notes:</b>
                <h6 class="text-secondary">{{$digest->note}}</h6>
            </h5>

        </div>
    </div>
  </div>
</div>
</body>
</html>
